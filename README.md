# README #

### What is this repository for? ###

This repository includes resources and results of the ICAPS 2020 paper: 
F. Percassi, A.E. Gerevini, E. Scala, I. Serina, M. Vallati, Generating and Exploiting Cost Predictions in Heuristic State-Space Planning, In Proceedings of the 30th International Conference on Automated Planning and Scheduling (ICAPS-20), 2020. 

